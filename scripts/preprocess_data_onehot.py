import os
import hydra
from omegaconf import DictConfig
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder

original_cwd = os.getcwd()

# Определение функции препроцессинга с параметрами конфигурации и путями файлов
def preprocess_data(input_file, output_train, output_test, cfg: DictConfig):

    data = pd.read_csv(input_file)
    hdf = data.copy()
    
    # Обработка категориальных признаков
    h_cat_en_bedr, h_categories_bedr = hdf['ocean_proximity'].factorize()
    oh = OneHotEncoder()
    coded_bedr = oh.fit_transform(h_cat_en_bedr.reshape(-1,1))
    proxy_bedr = pd.DataFrame(coded_bedr.toarray(), index = hdf.index, columns = ['1', '2', '3', '4', '5'])
    
    hdf = pd.concat([hdf, proxy_bedr], axis = 1)
    hdf = hdf.drop(['ocean_proximity'], axis = 1)
    hdf = hdf.dropna(subset=['median_house_value'])
    
    # Разделение данных на тренировочные и тестовые с параметрами из конфигурации
    train_set, test_set = train_test_split(hdf, test_size=cfg.preprocessing.preprocessing.test_size, random_state=cfg.preprocessing.preprocessing.random_state)
    train_set.to_csv(output_train, index=False)
    test_set.to_csv(output_test, index=False)

# Оболочка для интеграции Snakemake и Hydra
# @hydra.main(config_path="src/config", config_name="config", version_base=None)
@hydra.main(config_path=os.path.join(original_cwd, "src/config"), config_name="config", version_base=None)
def main(cfg: DictConfig):
    # Получение аргументов из Snakemake
    input_file = snakemake.input[0]
    output_train = snakemake.output.train
    output_test = snakemake.output.test
    preprocess_data(input_file, output_train, output_test, cfg)

if __name__ == "__main__":
    main()