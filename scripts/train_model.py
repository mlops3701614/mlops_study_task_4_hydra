import os
import hydra
from omegaconf import DictConfig, OmegaConf
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
import joblib


original_cwd = os.getcwd()

def train_model(input_file, output_file, cfg: DictConfig):
    data = pd.read_csv(input_file)
    X = data.drop('median_house_value', axis=1)
    y = data['median_house_value']
    
    model = RandomForestRegressor(n_estimators=cfg.training.training.n_estimators, max_depth=cfg.training.training.max_depth)
    model.fit(X, y)
    
    joblib.dump(model, output_file)


@hydra.main(config_path=os.path.join(original_cwd, "src/config"), config_name="config", version_base=None)
def main(cfg: DictConfig):
    input_file = snakemake.input.train
    output_file = snakemake.output.model
    train_model(input_file, output_file, cfg)

if __name__ == "__main__":
    main()