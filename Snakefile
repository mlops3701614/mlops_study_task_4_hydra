#ruleorder: train_model > preprocessing_data > download_data

# Определение списка наборов данных
DATASETS = {
        "housing_1": "https://drive.google.com/uc?export=download&id=1eBBwhfp0l7ro30_ELzOBVU22hoXn_Iuf",
        "housing_2": "https://drive.google.com/uc?export=download&id=190-3ruelz7W1XafUQlN_RWuThfZKhi6Q"}

rule all:
    input:
        expand(["data/raw_{dataset}.csv",
                "data/processed_onehot/{dataset}_train.csv",
                "data/processed_onehot/{dataset}_test.csv",
                "models/{dataset}_model.pkl"], dataset=DATASETS.keys())


rule download_data:
    output:
        "data/raw_{dataset}.csv"
    params:
        url = lambda wildcards: DATASETS[wildcards.dataset]
    threads: 2
    script:
        "scripts/download_data.py"

rule preprocessing_data_onehot:
    input:
        "data/raw_{dataset}.csv"
    output:
        train = "data/processed_onehot/{dataset}_train.csv",
        test = "data/processed_onehot/{dataset}_test.csv"
    threads: 2
    script:
        "scripts/preprocess_data_onehot.py"


rule train_model:
    input:
        train = "data/processed_onehot/{dataset}_train.csv"
    output:
        model = "models/{dataset}_model.pkl"
    threads: 2
    script:
        "scripts/train_model.py"