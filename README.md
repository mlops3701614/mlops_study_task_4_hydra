# MLOps Pipeline with Snakemake and Hydra
## Project Description

This project is an MLOps pipeline that utilizes Snakemake to automate data processing and machine learning model training workflows. Hydra is used for configuration management, allowing dynamic parameter adjustments without the need to modify the code.

Project Structure
- `data/`: Directory for storing raw and processed data.
- `models/`: Directory for saving trained machine learning models.
- `src/`: Contains Python scripts for data preprocessing and model training.
- `config/`: Directory with configuration files for Hydra.
- `Snakefile`: The main Snakemake file that describes the rules and dependencies of tasks in the pipeline.

## Usage
To run the pipeline, use the following command at the project root:

```bash
snakemake --cores [N]
```
Where [N] is the number of cores you wish to allocate for the workflow execution.

## Configurations
Examples of configurations for data preprocessing and model parameters are located in the config/ directory. Configurations can be modified to test different data processing and model training parameters.

## Key Components of the Pipeline
1. **Data Loading**: Scripts for downloading raw data.
2. **Data Preprocessing**: Scripts for processing data before training models. Includes various processing strategies controlled by Hydra configurations.
3. **Model Training**: Scripts for training and saving models. Model parameters are also managed through Hydra configurations.

## DAG Visualization
Below is the Directed Acyclic Graph (DAG) of the workflow, illustrating the dependencies between the tasks:
![My photo](dag.png)
